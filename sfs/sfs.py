import pysftp
import os
from getpass import getpass as getpass
from sys import argv as argv

try:
    if(argv[1] == "devmode"):
        passwd = getpass(prompt="password? ")
        uname = 'john'
        addr = '127.0.0.1'
        rem_dir = '/home/john/testdir1/'
        loc_dir = '/home/john/testdir2/'
        #0=go on mtime, 1=client to server 2=server to client, 3=sync mtimes
        trans_mode = 99
except:
    #get input, must be changed for python3
    uname = raw_input("username? ")
    passwd = getpass(prompt="password? ")
    addr = raw_input("address? ")
    #port = raw_input("port? ")
    rem_dir = raw_input("remote directory? ")
    loc_dir = raw_input("local directory? ")

cn = pysftp.CnOpts()

#lists/dicts to store values from walktree into
c_dirs = []
c_file = []
c_file2 = []
c_mtime = {}
s_dirs = []
s_file = []
s_file2 = []
s_mtime = {}

#make local lists/dictionaries of files/direcs
pysftp.walktree(loc_dir,c_file.append, c_dirs.append, c_file2.append)
c_file.sort()
c_file2.sort()
c_dirs.sort()

for a in range(0, len(c_dirs)):
    c_dirs[a] = c_dirs[a].lstrip(loc_dir)

#make dict for local files and mtimes
for a in range(0 ,len(c_file)):
    w_file = c_file[a]
    mtime = int(os.stat(w_file).st_mtime)
    w_file = w_file.lstrip(loc_dir)
    c_mtime[w_file] = mtime

with pysftp.Connection(addr, username=uname,password=passwd,cnopts=cn) as sftp:
    sftp.cd(rem_dir)
    sftp.walktree(rem_dir, s_file.append, s_dirs.append, s_file2.append)
    
    s_file.sort()
    s_file2.sort()
    s_dirs.sort()

    for a in range(0, len(s_dirs)):
        s_dirs[a] = s_dirs[a].lstrip(rem_dir)

    #make dict for server files and mtimes
    for a in range(0 ,len(s_file)):
        w_file = s_file[a]
        mtime = int(sftp.stat(w_file).st_mtime)
        w_file = w_file.lstrip(rem_dir)
        s_mtime[w_file] = mtime

    #find out what the files do(n't) have in common
    uniq_server = list(set(s_mtime.items()) - set(c_mtime.items()))
    uniq_client = list(set(c_mtime.items()) - set(s_mtime.items()))
    
    #find out what the directories don'te have in common
    uniq_dirs_client = set(c_dirs) - set(s_dirs)
    uniq_dirs_server = set(s_dirs) - set(c_dirs)
    
    if   trans_mode == 0:
        i=1
    elif trans_mode == 1:
        #put(preserve_mtime = True)
        for a in uniq_dirs_client:
            sftp.put_d(loc_dir + a, rem_dir, preserve_mtime=True)

        for a in uniq_client:
            sftp.put(loc_dir + a[0], rem_dir + a[0], preserve_mtime=True)

    elif trans_mode == 2:
        i=1
    elif trans_mode == 3:
        uniq_server = dict(uniq_server)
        changed_fs = 0
        for a in uniq_client:
            try:
                wfile = loc_dir + a[0]
                mtime = uniq_server[a[0]]
                os.utime(wfile, (mtime, mtime))
                changed_fs += 1
            except:
                continue
        print("Successfully changed the metadata of " + changed_fs + " files")
    else:
        i=1


print("-" * 80)
print(uniq_client)
print("-" * 80)
print(c_dirs)
