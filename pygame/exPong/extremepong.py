# This is a pong game that Chase Sill and Alex Crone have created, this is our first game that we have made that seems to be unique and good enough for it to be displayed online. Please enjoy playing the game as well as reading the code.
from pygame import display, draw, key, event, font, KEYDOWN, QUIT, image, K_UP, K_DOWN, K_LEFT, K_RIGHT, K_s
from time import sleep
display.init()
font.init()

window = display.set_mode((800,600))
greenblock = image.load("./greenblock.png").convert()
greenblock1 = image.load("./greenblock1.png").convert()
background = image.load("./metal_4.png").convert()
# Make our colors
grassgreen = (0,180,0)
blue = (0,0,255)
red = (255,0,0)
white = (255,255,255)
black = (0,0,0)
# Define our variables
# Human verticle paddle variables
px = 10
py = 350
pw = 20		
ph = 100
# Human horizontal paddle variables
px1 = 350
py1 = 20
pw1 = 100
ph1 = 20
# Computer verticle paddle variables
cpx = 770
cpy = 350
cpw = 20
cph = 100
# Computer horizontal paddle variables
cx1 = 350
cy1 = 570
cw1 = 100
ch1 = 20
# Ball variables
bvx = 0
bvy = 0
bspeed = 5.3 # ball speed
pspeed = 5 # our paddle speed
hscore = 0
cscore = 0
served = False
gameover = False

# Surfaces, fonts, misc goo
window = display.set_mode((800,600))
cfont = font.Font(None,18)
sfont = font.Font(None,24)
ssfont = font.Font(None,15)
bfont = font.Font(None,80)
isfont = font.Font(None,20)
ins = isfont.render("you control left & top paddles",1,red)
ins2 = isfont.render("computer controls right & bottom paddles",1,red) 
hscoreM = sfont.render("SCORE: 0",1,red)
cscoreM = sfont.render("SCORE: 0",1,red)
gameoverM = bfont.render("GAMEOVER",1,red)
opening1 = sfont.render("use the arrow keys to move",1,red)
opening2 = sfont.render("press space bar to launch ball",1,red)
opening3 = bfont.render("Hit s to Start Game",1,grassgreen)
credit1 = cfont.render("by Chase Sill and Alex Crone",1,red)
credit2 = cfont.render("Special thanks to Michael Surran, our",1,red)
credit3 = cfont.render("computer mentor.",1,red)
win = bfont.render("YOU WIN",1,red)
silacron = ssfont.render("produced by, SilaCron programming",1,blue)
opening = True
key.set_repeat(1,1)

# Opening Sequence
while opening:
	action = event.poll()
	if action.type == KEYDOWN:
		if action.key == K_s:
			opening = False
	window.blit(silacron,(20,5))
	display.update()
	window.blit(ins,(300,160))
	display.update()
	window.blit(ins2,(270,175))
	display.update()
	window.blit(opening1,(290,200))
	display.update()
	window.blit(opening2,(278,230))
	display.update()
	window.blit(credit1,(100,500))
	display.update()
	window.blit(credit2,(100,520))
	window.blit(credit3,(120,535))
	display.update()
	window.blit(opening3,(175,300))
	display.update()
# Main program loop - here we go!!!
while not gameover:
        sleep(.016)
	# take care of events first
	action = event.poll()
	if action.type == KEYDOWN:
		if action.key == K_UP and py > 0:
			py -= pspeed
		if action.key == K_DOWN and py + ph < 600:
			py += pspeed
		if action.key == K_LEFT:
			px1 -= 4.85
		if action.key == K_RIGHT:
			px1 += 4.85
		if action.key == 32 and not served:
			served = True
			bx = int(px + pw + 5)
			by = int(py + ph / 2)
                        #PAY ATTENTION TO HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
			bvx = bspeed
			bvy = 0
		if action.key == 27: # esc
			gameover = True
	if action.type == QUIT:
		gameover = True
		
	# Start drawing game
	#window.fill(black)
	window.blit(background,(0,0))
	window.blit(greenblock,(px,py))
	window.blit(greenblock,(cpx,cpy))
	window.blit(greenblock1,(px1,py1))
	window.blit(greenblock1,(cx1,cy1))
	window.blit(hscoreM,(20,20))
	window.blit(cscoreM,(700,20))
	window.blit(silacron,(20,5))
	# Stop paddles from going off the screen
	if py < 0 or py > 600:
		py = -py
	if cy1 < 0 or cy1 > 600:
		cy = -cy
	if px1+100 > 800:
		px1 -= 4.2
	if px1 < 0:
		px1 = -px1
	if cx1+100 > 800:
		cx1 -= 4.2
	if cx1 < 0:
		cx1 = -cx1
	# Only draw ball if served
	if served:
		draw.circle(window,white,(bx,by),5)
		# Now is time for collision detection, scoring, etc!
		bx = int(bx +bvx)
		by = int(by +bvy)
		if bx > 800 or by > 600:
			# We score!
			hscore += 1
			hscoreM = sfont.render("SCORE: %i"%hscore,1,red)
			served = False
			if hscore == 10:
				window.blit(win,(300,300))
				display.update()
				sleep(3)
				gameover = True
		if bx < 0 or by < 0:
			# Computer scores
			cscore += 1
			cscoreM = sfont.render("SCORE: %i"%cscore,1,red)
			served = False
			if cscore == 10:
				window.blit(gameoverM,(240,250))
				display.update()
				sleep(3)
				gameover = True
			
		# Collision Detection
		if bx > px1 and bx < px1+pw1 and by > py1 and by < py1+ph1:
			bvy = -bvy
		if bx > cx1 and bx < cx1+cw1 and by > cy1 and by < cy1+ch1:
			bvy = -bvy
		if bx > px and bx < px+pw and by > py and by < py+ph:
			bvx = -bvx
			
			# put "spin" on ball
			bvy = (by - py - ph / 2) / 6
		if bx > cpx and bx < cpx+cpw and by > cpy and by < cpy+cph:
			bvx = -bvx
			bvy = (by - cpy - cph / 2) / 6
			
		# ARTIFICIAL INTELLIGENCE
		if cpy + cph / 2 - 10 > by:
			cpy -= 4
		if cpy + cph / 2 + 10 < by:
			cpy += 4
		if cx1 + cw1 / 2 - 50 > bx:
			cx1 -= 4.85
		if cx1 + cw1 / 2 - 50 < bx:
			cx1 += 4.85
			
	display.update()
