#!/usr/bin/python

from sys import argv

def findPrimes(ending, starting=2, directPrint=False):
    nums = list(range(starting,ending+1))
    for testNum in nums:
        for checkNum in nums:
            if checkNum % testNum == 0:
                del(checkNum)

if __name__ == "__main__":
    if len(argv) == 3:
        try:
            start = int(argv[1])
            upTo = int(argv[2])
        except:
            print("Error in converting arguments to integer.")
    elif len(argv) == 2:
        try:
            upTo = int(argv[1])
            start = 2
        except:
            print("Error in converting arguments to integer.")
    else:
        start = 2
        upTo = 100
    
    findPrimes(upTo, start, True)
