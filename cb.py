#!/usr/bin/python2
import random

#bool for main loop and secret number that player has to guess
gameWon = False
secret = random.randint(1, 9999)

#convert nubmer to string and make it four characters long and make it a list
secret = str(secret)
while len(secret) < 4:    
    secret = '0' + secret
secret = list(secret)

while not(gameWon):
    #print("Enter a four digit number.")
    guess = raw_input("> ")

    #makes sure that guess entered can be a number
    try:
        int(guess)
    except:
        print("invalid entry")
        continue

    #makes sure it is the right length - if less than 4, add zeroes to front
    if len(guess.strip()) > 4:
        print("invalid entry")
    elif (len(guess.strip()) < 4):
            while (len(guess) < 4):
                guess = '0' + guess
    
    #make list to match secret
    guess = list(guess)

    if guess == secret:
        print("You Win!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        gameWon = True
        exit()

    cows, bulls = 0, 0
    
    #checks for bulls
    for c in range(0,4):
        if guess[c] == secret[c]:
            bulls += 1
            guess[c] = 'X'

    #checks for cows, goes through digits of secret one by one and checks if and digits of guess match it
    for b in range(0,4):
        for h in range(0,4):
            if guess[h] == secret[b]:
                cows += 1
                guess[h] = 'X'

    #print(("secret: %s") % (secret))
    #print(("guess:  %s") % (guess))

    print(("Cows: %s | Bulls: %s") % (cows, bulls))

