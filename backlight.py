#!/usr/bin/python

from sys import argv 
from os import popen
levels = [0,1,2,4,8,15,25,37,50,75,100]

#get current backlight
backlight = float(popen("xbacklight").read())

for i in range(0,len(levels)):
    if levels[i] >= backlight:
        backlight = i
        break

if (argv[1] == "u" and backlight != 10): 
   #output new backlight val
   popen(("xbacklight -set " + str(levels[i+1])))
elif(argv[1] == "d" and backlight != 0):
   popen(("xbacklight -set " + str(levels[i-1])))


