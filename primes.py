#!/usr/bin/python

from sys import argv

def findPrimes(ending, starting=2, directPrint=False):
    primes = []
    for i in range(starting, ending+1):
        isPrime = True
        for j in range(2, i/2+1):
            if i%j == 0:
                isPrime = False
                break

        if isPrime:
            if directPrint:
                print(i)
            else:
                primes.append(i)

    return primes

if __name__ == "__main__":
    if len(argv) == 3:
        try:
            start = int(argv[1])
            upTo = int(argv[2])
        except:
            print("Error in converting arguments to integer.")
    elif len(argv) == 2:
        try:
            upTo = int(argv[1])
            start = 2
        except:
            print("Error in converting arguments to integer.")
    else:
        start = 2
        upTo = 100
    
    findPrimes(upTo, start, True)
