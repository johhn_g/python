#!/usr/bin/python

from sys import argv

def print_box(ifile,center=False):
    #gets the text too put in box
    sentences = []

    def get_text():
        sentence=raw_input("> ")
        if sentence != "":
            sentence = sentence.strip()
            sentences.append(sentence)
            get_text()
        else:
            if sentences[-1] != "":
                sentences.append("")
                get_text()
            else: 
                del sentences[-1]
    
    def trim_b_lines():
        if sentences[-1] == "":
            del sentences[-1]
            trim_b_lines()
        elif sentences[1] =="":
            del sentences[1]
            trim_b_lines()

    if ifile:
        try:    
            sentences = open(ifile).read().splitlines()
            trim_b_lines()
        except:
            sentences = ifile.splitlines()
            #trim_b_lines()
    else:
        get_text()
    
    #determines how long the lline(longest line) is
    lline = 0
    for line in sentences:
        if len(line) >= lline:
            lline = len(line)
    
    print "+" + "-" * (lline + 2) + "+"
    for sentence in sentences:
        if center:
            space = lline - len(sentence)
            line = "| " + (" " * (space/2)) + sentence + (" " * (space/2 + space%2)) + " |"
            print line
        else:
            line = "| " + sentence + (" " * (lline - len(sentence))) + " |"
            print line
    print "+" + "-" * (lline + 2) + "+"

if __name__ == "__main__":
    if len(argv) == 2:
        if argv[1] == "-c":
            print_box(False,True)
        else:
            print_box(argv[1], False)
    elif len(argv) >= 3:
        ycenter = False
        for files in argv:
            if files == "-c":
                ycenter = True
        for files in range(1, len(argv)):
            if argv[files] == "-c":
                continue
            print_box(argv[files], ycenter)
    else:
        print_box(False)
